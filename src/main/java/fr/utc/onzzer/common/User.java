package fr.utc.onzzer.common;

import java.util.UUID;

public class User {

    private final UUID uuid;
    private final String username, password;

    public User(UUID uuid, String username, String password) {
        this.uuid = uuid;
        this.username = username;
        this.password = password;
    }

    public UUID getUUID() {
        return this.uuid;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }
}
